import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle; // leer json

class _MenuProvider {
  String url = 'data/menu_opts.json';
  List<dynamic> opciones = [];

  _MenuProvider() {
    // cargarData();
  }

  Future<List<dynamic>> cargarData() async {
    final response = await rootBundle.loadString(url);
    Map dataMap = json.decode(response); // convierte string a json
    opciones = dataMap['rutas'];
    print(dataMap['rutas']);

    return opciones;
  }
}

final menuProvider = _MenuProvider();

import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  // const HomePageTemp({super.key});

  final opciones = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Components temp app'),
      ),
      body: ListView(children: _crearItems1()),
    );
  }

  List<Widget> _crearItems() {
    // creando lista:
    List<Widget> lista = [];
    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
        subtitle: Text('Hola mundo'),
      );
      lista
        ..add(tempWidget)
        ..add(Divider());
    }
    return lista;
  }

  List<Widget> _crearItems1() {
    return opciones
        .map((opt) => Column(
              children: [
                ListTile(
                  title: Text(opt),
                  subtitle: Text('Hola Mundo'),
                  leading: Icon(Icons.account_balance_wallet),
                  trailing: Icon(Icons.keyboard_arrow_right),
                  onTap: () {},
                ),
                Divider()
              ],
            ))
        .toList();
  }

  List<Widget> _crearItems2() {
    List<Widget> items = [];

    for (String opt in opciones) {
      items.add(ListTile(
        title: Text(opt),
      ));

      // Agregar un Divider después de cada ListTile, excepto el último.
      if (opciones.last != opt) {
        items.add(Divider());
      }
    }

    return items;
  }
}

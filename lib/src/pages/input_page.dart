import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  const InputPage({super.key});

  @override
  State<InputPage> createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email = '';
  String _password = '';
  String _fecha = '';
  String _selectOpt = '';
  List<String> _poderes = ['Volar', 'Rayos X', 'Fuerza'];
  TextEditingController _inputFielDateController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _selectOpt = _poderes[0]; // Establece el primer valor como predeterminado
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs Page'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: <Widget>[
          _crearInput(),
          Divider(),
          _crearInputEmail(),
          Divider(),
          _crearInputPass(),
          Divider(),
          _crearInputFecha(context),
          Divider(),
          _crearSelect(),
          Divider(),
          _crearPersona()
        ],
      ),
    );
  }

  Widget _crearInput() {
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization
          .sentences, // al hacer espacio se hacer el upperCase
      decoration: InputDecoration(
          counter: Text('Letras ${_nombre.length}'),
          hintText: 'Nombre de la Persona',
          labelText: 'Nombre',
          helperText: 'Solo es nombre',
          suffixIcon: Icon(Icons.accessibility),
          icon: Icon(Icons.account_circle),
          border: OutlineInputBorder()),
      onChanged: (value) {
        setState(() {
          _nombre = value;
        });
        // print(value);
      },
    );
  }

  Widget _crearInputEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          hintText: 'Email de la Persona',
          labelText: 'Email',
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email),
          border: OutlineInputBorder()),
      onChanged: (value) {
        setState(() {
          _email = value;
        });
        // print(value);
      },
    );
  }

  Widget _crearInputPass() {
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
          hintText: 'Password',
          labelText: 'Password',
          suffixIcon: Icon(Icons.lock_open),
          icon: Icon(Icons.lock),
          border: OutlineInputBorder()),
      onChanged: (value) {
        setState(() {
          _password = value;
        });
        // print(value);
      },
    );
  }

  Widget _crearInputFecha(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      controller: _inputFielDateController,
      decoration: InputDecoration(
          hintText: 'Fecha de nacimiento',
          labelText: 'Fecha de nacimiento',
          suffixIcon: Icon(Icons.perm_contact_calendar),
          icon: Icon(Icons.calendar_today),
          border: OutlineInputBorder()),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectInputDate(context);
      },
    );
  }

  _selectInputDate(BuildContext context) async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2018),
        lastDate: new DateTime(2025),
        locale: Locale('es', 'ES'));
    if (picked != null) {
      setState(() {
        _fecha = picked.toString();
        _inputFielDateController.text = _fecha;
      });
    }
  }

/**
 * @Description: Genera combo tipo select
*/
  Widget _crearSelect() {
    return Row(
      children: [
        Icon(Icons.select_all),
        SizedBox(
          width: 30.0,
        ),
        Expanded(
          child: DropdownButton(
              value: _selectOpt,
              items: _getOpcionesSelect(),
              onChanged: (opt) {
                // print(opt);
                setState(() {
                  _selectOpt = opt.toString();
                });
              }),
        ),
      ],
    );
  }

  List<DropdownMenuItem<String>> _getOpcionesSelect() {
    List<DropdownMenuItem<String>> lista = [];
    _poderes.forEach((poder) {
      lista.add(DropdownMenuItem(
        child: Text(poder),
        value: poder,
      ));
    });
    return lista;
  }

  Widget _crearPersona() {
    return ListTile(
      title: Text('Nombre: $_nombre'),
      subtitle: Text('Email: $_email'),
      trailing: Text('Poder: $_selectOpt'),
    );
  }
}

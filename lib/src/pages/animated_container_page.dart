import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  const AnimatedContainerPage({super.key});

  @override
  State<AnimatedContainerPage> createState() => _AnimatedContainerPageState();
}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {
  double _width = 50.0;
  double _height = 50.0;
  Color _color = Colors.pink;
  BorderRadiusGeometry _borderRadius = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
      ),
      body: Center(
        child: AnimatedContainer(
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            borderRadius: _borderRadius,
            color: _color,
          ),
          duration: Duration(seconds: 1), // tiempo de la animacion
          curve: Curves
              .fastOutSlowIn, // animaciones modelos: https://api.flutter.dev/flutter/animation/Curves-class.html
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.restart_alt),
        onPressed: _reloadImage,
        tooltip: 'Reload',
      ),
    );
  }

  void _reloadImage() {
    final random = Random(); // funcion para obtener numeros random
    setState(() {
      _width = random.nextInt(300).toDouble(); // ancho max 300
      _height = random.nextInt(300).toDouble(); // alto max 300
      _color = Color.fromRGBO(random.nextInt(255), random.nextInt(255),
          random.nextInt(255), 1); // colores aleatorios
      _borderRadius = BorderRadius.circular(
          random.nextInt(100).toDouble()); // borde redondeado max 100
    });
  }
}

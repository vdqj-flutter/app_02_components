import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

// stfull
class SliderPage extends StatefulWidget {
  const SliderPage({super.key});

  @override
  State<SliderPage> createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorSlider = 100.0;
  bool _bloquearCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sliders'),
      ),
      body: Container(
          padding: EdgeInsets.only(top: 50.0),
          child: Column(
            children: [
              _crearSlider(),
              Divider(),
              _crearCheckBox(),
              Divider(),
              _crearSwitch(),
              Divider(),
              Expanded(child: _crearImagen()),
            ],
          )),
    );
  }

  Widget _crearSlider() {
    return Slider(
        activeColor: Colors.indigoAccent,
        label: 'Tamaño de la imagen',
        value: _valorSlider,
        min: 10,
        max: 400,
        onChanged: (_bloquearCheck)
            ? null
            : (valor) {
                // print(valor);
                setState(() {
                  _valorSlider = valor;
                });
              });
  }

  Widget _crearImagen() {
    return Image(
      image: NetworkImage('https://fondosmil.com/fondo/72431.jpg'),
      width: _valorSlider,
      fit: BoxFit.contain, // para que la imagen no se salga del margen
    );
  }

  /**
   * @Description: Crear checkbox para bloquear re-size de imagen
  */
  Widget _crearCheckBox() {
    // return Checkbox(
    //     value: _bloquearCheck,
    //     onChanged: (valor) {
    //       if (valor != null) {
    //         // print('si: $valor');
    //         setState(() {
    //           _bloquearCheck = valor;
    //         });
    //       }
    //     });
    return CheckboxListTile(
        title: Text('Bloquear slice?'),
        value: _bloquearCheck,
        onChanged: (valor) {
          if (valor != null) {
            // print('si: $valor');
            setState(() {
              _bloquearCheck = valor;
            });
          }
        });
  }

  /**
   * @Description: Crear switch para bloquear re-size de imagen
  */
  Widget _crearSwitch() {
    return SwitchListTile(
        title: Text('Bloquear slice?'),
        value: _bloquearCheck,
        onChanged: (valor) {
          if (valor != null) {
            // print('si: $valor');
            setState(() {
              _bloquearCheck = valor;
            });
          }
        });
  }
}

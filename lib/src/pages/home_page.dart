// importm:
import 'package:app_02_components/src/pages/alert_page.dart';
import 'package:app_02_components/src/providers/menu_provider.dart';
import 'package:app_02_components/src/utils/icon_string_util.dart';
import 'package:flutter/material.dart';

// stless:
class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Components'),
        centerTitle: true,
        elevation: 1.4,
      ),
      body: _lista(context),
    );
  }

  Widget _lista(BuildContext context) {
    // menuProvider.cargarData()

    // return ListView(
    //   children: _listaItems(),
    // );
    return FutureBuilder(
        future: menuProvider.cargarData(),
        initialData: [],
        builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
          print('snap: ');
          print(snapshot.data);

          return ListView(
            children: _listaItems(snapshot.data ?? [], context),
          );
        });
  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    final List<Widget> opciones = [];
    data.forEach((opt) {
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        // leading: Icon(Icons.account_circle, color: Colors.blue),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {
          // final route = MaterialPageRoute(builder: (context) {
          //   return AlertPage();
          // });
          // Navigator.push(context, route);
          Navigator.pushNamed(context, opt['ruta']);
        },
      );
      opciones
        ..add(widgetTemp)
        ..add(Divider());
    });
    return opciones;
  }
}

# app_02_components

Este proyecto fue realizado por Victor Daniel Quino Jimenez.
Fecha: 2023-09-05

## Content in proyect

Proyecto en el que se pueden apreciar:

1. muestra de alertas
2. avatar page: carga de imagen con loading
3. cards
4. animacion de un cuadrado
5. formularios con input, email, pass, date, select
6. sliders: se modifica el tamaño de un imagen con switch y checkbox
7. lista de imagenes con carga dinamica con scroll final e inicio.

## Getting Started

Para el desarrollo de este proyecto se uso Visual Studio Code.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
